#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>

using namespace cv;
using namespace std;
Mat img, src_gray;
Mat dst, detected_edges;
int lowThreshold = 10;
const int max_lowThreshold = 100;
const int ratio = 2;
const int kernel_size = 3;
const char* window_name = "Edge Map";

//Color Thresholding
void HSVfiltering(Mat img) {
	Mat blurredImage;
	GaussianBlur(img, blurredImage, Size(3, 3), 0);
	Mat output;
	cvtColor(blurredImage, output, COLOR_BGR2HSV);

	Scalar minThresh = Scalar(0, 0, 0);
	Scalar maxThresh = Scalar(100, 255, 255);

	Mat mask, imageOut, inverseMask;
	inRange(output, minThresh, maxThresh, mask);
	bitwise_not(mask, inverseMask);
	bitwise_and(img, img, imageOut, inverseMask);
	//imwrite("C:/opencv_images/rat3filtered.jpg", imageOut);

	imshow("Mask", mask);
	imshow("Rat Image", imageOut);
	waitKey(0);
}

//Edge detection
int detector(Mat src)
{

	img = src;
	if (src.empty())
	{
		std::cout << "Could not open or find the image!\n" << std::endl;
		return -1;
	}
	dst.create(src.size(), src.type());
	cvtColor(src, src_gray, COLOR_BGR2GRAY);
	namedWindow(window_name, WINDOW_AUTOSIZE);
	blur(src_gray, detected_edges, Size(25, 25));
	Canny(detected_edges, detected_edges, lowThreshold, lowThreshold * ratio, kernel_size);
	dst = Scalar::all(0);
	img.copyTo(dst, detected_edges);
	imshow(window_name, dst);
	imwrite("C:/opencv_images/rat1CannyEdge.jpg", dst);
	waitKey(0);
	return 0;
}

//Contouring
Mat contours_full(Mat src) {
	Mat imgGray, imgBlur, edges, dilated;
	Mat contoured = Mat::zeros(src.rows, src.cols, CV_8UC3);
	Mat kernel = getStructuringElement(MORPH_RECT, Size(3, 3));
	vector<vector<Point>> contours;
	vector<Vec4i> hierarchy;

	cvtColor(src, imgGray, COLOR_BGR2GRAY);
	blur(imgGray, imgBlur, Size(15, 15));
	//imwrite("C:/opencv_images/rat3UnderBlurred.jpg", imgBlur);
	Canny(imgBlur, edges, 10, 30);
	dilate(edges, dilated, kernel);

	findContours(dilated,
		contours,
		hierarchy,
		RETR_EXTERNAL,
		CHAIN_APPROX_SIMPLE);

	// iterate through all the top-level contours,
	// draw each connected component with its own random color
	int idx = 0;
	for (; idx >= 0; idx = hierarchy[idx][0])
	{
		Scalar color(rand() & 255, rand() & 255, rand() & 255);
		drawContours(contoured, contours, idx, color, FILLED, 8, hierarchy);
	}
	imshow("Components", contoured);
	//imwrite("C:/opencv_images/rat3Contours.jpg", contoured);
	waitKey(0);
	return contoured;
}