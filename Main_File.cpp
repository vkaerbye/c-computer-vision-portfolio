#include "boatDetector.h"
#include "RatFiltering.h"
#include "beerDetector.h"

int main()
{
    Mat img = imread("./beer_images/beer4_1.jpg");
    cout << "The beer depicted is: " << matchBeersSiftBW(img) << endl;
    
    return 0;
}