#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/features2d.hpp>
#include <filesystem>
#include <iostream>
#include <list>

using namespace cv;
using namespace std;
namespace fs = filesystem;
using recursive_directory_iterator = std::filesystem::recursive_directory_iterator;

//Function for creating binary image of contours in an image
Mat create_template(Mat src, string fname) {
	Mat imgGray, imgBlur, edges, dilated;
	Mat contoured = Mat::zeros(src.rows, src.cols, CV_8UC3);
	Mat kernel = getStructuringElement(MORPH_RECT, Size(2, 2));
	vector<vector<Point>> contours;
	vector<Vec4i> hierarchy;

	cvtColor(src, imgGray, COLOR_BGR2GRAY);
	GaussianBlur(imgGray, imgBlur, Size(3, 3), 3, 0);
	Canny(imgBlur, edges, 235, 250);
	dilate(edges, dilated, kernel);
	findContours(dilated,
		contours,
		hierarchy,
		RETR_EXTERNAL,
		CHAIN_APPROX_SIMPLE);
	cout << "Number of contours found: " << contours.size() << endl;
	// iterate through all the top-level contours,
	// draw each connected component with its own random color
	int idx = 0;
	for (; idx >= 0; idx = hierarchy[idx][0])
	{
		Scalar color(255, 255, 255);
		drawContours(contoured, contours, idx, color, FILLED, 8, hierarchy);
	}
	//imshow("Components", contoured);
	imwrite("./extractedBeerContours/"+fname+".jpg", contoured);
	cout << "Wrote image successfully" << endl;
	
	return contoured;
}

std::string get_stem(const fs::path& p) { return p.stem().string(); }

//DISCONTINUED: Contour matching based on binary image matrix from contours
string matchBeersBinary(Mat src) {
	double ans = 0;
	Mat mask;
	Scalar minThresh = Scalar(250, 250, 250);
	Scalar maxThresh = Scalar(255, 255, 255);
	Mat templateList[6];
	int count = 0;
	for (const auto& dirEntry : recursive_directory_iterator("./beerTemplates/")) {
		string fname = "./beerTemplates/" + get_stem(dirEntry.path()) + ".jpg";
		Mat image = imread(fname);
		inRange(image, minThresh, maxThresh, mask);
		templateList[count] = mask;
		count++;
		cout << "Converted: " << dirEntry << std::endl;
	}

	Mat imgGray, imgBlur, edges, dilated, eroded, binaryImage;
	Mat contoured = Mat::zeros(src.rows, src.cols, CV_8UC3);
	Mat kernel = getStructuringElement(MORPH_RECT, Size(2, 2));
	vector<vector<Point>> contours;
	vector<Vec4i> hierarchy;

	cvtColor(src, imgGray, COLOR_BGR2GRAY);
	GaussianBlur(imgGray, imgBlur, Size(3, 3), 3, 0);
	Canny(imgBlur, edges, 235, 250);
	dilate(edges, dilated, kernel);
	erode(dilated, eroded, kernel);
	findContours(eroded,
		contours,
		hierarchy,
		RETR_EXTERNAL,
		CHAIN_APPROX_SIMPLE);
	cout << "Number of contours found: " << contours.size() << endl;
	int idx = 0;
	for (; idx >= 0; idx = hierarchy[idx][0])
	{
		Scalar color(255, 255, 255);
		drawContours(contoured, contours, idx, color, FILLED, 8, hierarchy);
	}
	
	inRange(contoured, minThresh, maxThresh, binaryImage);
	double results[6];
	double smallestNum = 1000;
	for (int l = 0; l < 6; l++) {
		ans = matchShapes(templateList[l], binaryImage, CONTOURS_MATCH_I1, 0);
		results[l] = ans;
		if (ans < smallestNum) smallestNum = ans;
		cout << ans << " " << endl;
	}
	string beerName;
	if (smallestNum == results[0]) beerName = "Albani Odense Classic";
	if (smallestNum == results[1]) beerName = "Tuborg Gr�n";
	if (smallestNum == results[2]) beerName = "Carlsberg Pilsner";
	if (smallestNum == results[3]) beerName = "Anarkist Bloody Weizen";
	if (smallestNum == results[4]) beerName = "Westmalle Trappist Dubbel";
	if (smallestNum == results[5]) beerName = "Slots Classic";

	return(beerName);
}

//Function to extract contours from image
vector<vector<Point>> extractContours(Mat src) {
	Mat tempGray, tempBlur, tempEdges, tempDilated, tempEroded;
	Mat contoured = Mat::zeros(src.rows, src.cols, CV_8UC3);
	Mat kernel = getStructuringElement(MORPH_RECT, Size(2, 2));
	vector<vector<Point>> contours;
	vector<Vec4i> hierarchy;
	cvtColor(src, tempGray, COLOR_BGR2GRAY);
	GaussianBlur(tempGray, tempBlur, Size(3, 3), 3, 0);
	Canny(tempBlur, tempEdges, 235, 250);
	dilate(tempEdges, tempDilated, kernel);
	//erode(tempDilated, tempEroded, kernel);
	findContours(tempDilated,
		contours,
		hierarchy,
		RETR_EXTERNAL,
		CHAIN_APPROX_SIMPLE);
	return contours;
}

//Function comparing beers based on their contours (DOESN'T SCALE TO MORE THAN 7 TEMPLATES YET)
string matchBeers(Mat src) {
	vector<double> ans;
	vector<vector<Point>> templateList[7];
	int count = 0;
	//Extracts the contours of all templates
	for (const auto& dirEntry : recursive_directory_iterator("./beerTemplates/")) {
		string fname = "./beerTemplates/" + get_stem(dirEntry.path()) + ".jpg";
		Mat image = imread(fname);
		templateList[count] = extractContours(image);
		count++;
		cout << "Converted: " << dirEntry << std::endl;
	}

	//Extracts the contours of the image
	vector<vector<Point>> imageContours = extractContours(src);
	cout << "Loaded image successfully and found " << to_string(imageContours.size()) << " Contours" << endl;

	
	//Compares image to contours 
	double results[7];
	double smallestNum = 1000;
	for (int l = 0; l < 7; l++) {
		if (imageContours.size() < templateList[l].size()) {
			for (int k = 0; k < imageContours.size(); k++) {
				double sim = matchShapes(templateList[l][k], imageContours[k], CONTOURS_MATCH_I1, 0);
				if(sim < 1000) ans.push_back(sim);
			}
		}
		else {
			for (int k = 0; k < templateList[l].size(); k++) {
				double sim = matchShapes(templateList[l][k], imageContours[k], CONTOURS_MATCH_I1, 0);
				if (sim < 1000) ans.push_back(sim);
			}
		}
		

		double sum = 0;
		for (int p = 0; p < ans.size(); p++) {
			//cout << "Contour likeness (ans[p]): " << to_string(ans[p]) << endl;
			sum += ans[p];
		}
		results[l] = sum / ans.size();
		if (results[l] < smallestNum) smallestNum = results[l];
		ans.clear();
		cout << results[l] << " " << endl;
	}

	string beerName;
	if (smallestNum == results[0]) beerName = "Albani Odense Classic";
	if (smallestNum == results[1]) beerName = "Tuborg Gr�n";
	if (smallestNum == results[2]) beerName = "Carlsberg Pilsner";
	if (smallestNum == results[3]) beerName = "Anarkist Bloody Weizen";
	if (smallestNum == results[4]) beerName = "Westmalle Trappist Dubbel";
	if (smallestNum == results[5]) beerName = "Slots Classic";
	if (smallestNum == results[6]) beerName = "Chouffe Mc Chouffe";
	
	return(beerName);
}

//Function using SIFT to compare an image to a set of templates (ALMOST SCALES WITH NUMBER OF TEMPLATES)
string matchBeersSiftBW(Mat img)
{
	Mat image, imgBW;
	cvtColor(img, imgBW, COLOR_BGR2GRAY);
	image = imgBW;

	//Check if image has loaded
	if (!image.data)
	{
		cout << "Error reading the image" << endl;
		return "The File could not be loaded";
	}

	//Detect keypoints on templates using SIFT detector
	Ptr<SIFT> detector = SIFT::create();
	vector<KeyPoint> keypoints_image;
	vector<KeyPoint> keypoints_templ;
	vector<Mat> keypoints_templ_list;
	Mat descriptors_image, descriptors_templ;
	int count = 0;
	for (const auto& dirEntry : recursive_directory_iterator("./beerTemplates/")) {
		string fname = "./beerTemplates/" + get_stem(dirEntry.path()) + ".jpg";
		Mat image = imread(fname, COLOR_BGR2GRAY);
		detector->detect(image, keypoints_templ);
		detector->compute(image, keypoints_templ, descriptors_templ);
		keypoints_templ_list.push_back(descriptors_templ);
		keypoints_templ.clear();
		count++;
		cout << "Converted: " << dirEntry << std::endl;
	}

	//Process input image and calculate descriptors
	detector->detect(image, keypoints_image);
	detector->compute(image, keypoints_image, descriptors_image);
	

	//Matching descriptors of input image and all templates
	FlannBasedMatcher matcher;
	vector<DMatch> good_matches;
	vector<vector<DMatch>> good_matches_list;
	for (int t = 0; t < keypoints_templ_list.size(); t++) {
		cout << "Comparing image to template " << to_string(t + 1) << "..." << endl;
		vector<DMatch> matches;
		matcher.match(descriptors_image, keypoints_templ_list[t], matches);


		double max_dist = 0; double min_dist = 100;
		//Calculate min and max distances between keypoints
		for (int i = 0; i < descriptors_image.rows; i++)
		{
			double dist = matches[i].distance;
			if (dist < min_dist) min_dist = dist;
			if (dist > max_dist) max_dist = dist;
		}

		//Save only good matches
		for (int i = 0; i < descriptors_image.rows; i++)
		{
			if (matches[i].distance <= max(2 * min_dist, 0.02))
			{
				good_matches.push_back(matches[i]);
			}
		}
		good_matches_list.push_back(good_matches);
		good_matches.clear();
	}
	//Calculate highest number of good matches
	int high_match = 0;
	for (int m = 0; m < good_matches_list.size(); m++) {
		cout << "Good Matches on template " << to_string(m+1) << ": " << good_matches_list[m].size() << endl;
		if (good_matches_list[m].size() > high_match) high_match = good_matches_list[m].size();
	}
	//Get beer name
	string beerName;
	if (high_match == good_matches_list[0].size()) beerName = "Albani Odense Classic";
	if (high_match == good_matches_list[1].size()) beerName = "Tuborg Gr�n";
	if (high_match == good_matches_list[2].size()) beerName = "Carlsberg Pilsner";
	if (high_match == good_matches_list[3].size()) beerName = "Anarkist Bloody Weizen";
	if (high_match == good_matches_list[4].size()) beerName = "Westmalle Trappist Dubbel";
	if (high_match == good_matches_list[5].size()) beerName = "Slots Classic";
	if (high_match == good_matches_list[6].size()) beerName = "Chouffe Mc Chouffe";
	waitKey(0);

	return beerName;
}