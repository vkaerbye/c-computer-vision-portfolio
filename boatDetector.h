#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/features2d.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/core/ocl.hpp>
#include <iostream>
#include "chains.h"

using namespace cv;
using namespace std;

//Function for object detection on a video-file
void counter() {
	VideoCapture cap("C:/opencv_images/boats_cropped.mp4");
	int framewidth = cap.get(CAP_PROP_FRAME_WIDTH);
	int frameheight = cap.get(CAP_PROP_FRAME_HEIGHT);
	const string fname = "C:/opencv_images/boats_contours_outlined.mp4";
	cv::VideoWriter output(fname, VideoWriter::fourcc('M', 'J', 'P', 'G'),29, Size(framewidth, frameheight));
	Mat frame;
	vector<vector<Point>> contours;
	vector<Vec4i> hierarchy;
	int fps = cap.get(CAP_PROP_FPS);
	cout << "VIDEO FPS:" << fps << endl;
	namedWindow("Contours", WINDOW_AUTOSIZE);
	if (!cap.isOpened()) { 
		cerr << "Could not open video file!" << endl;
		exit(0); 
	}
	while (cap.read(frame)) {
		Mat contoured = Mat::zeros(frame.rows, frame.cols, CV_8UC3);
		Mat proc = preprocessing(frame);
		findContours(proc,
			contours,
			hierarchy,
			RETR_EXTERNAL,
			CHAIN_APPROX_SIMPLE);

		int idx = 0;
		for (; idx >= 0; idx = hierarchy[idx][0])
		{
			Scalar color(rand() & 255, rand() & 255, rand() & 255);
			drawContours(contoured, contours, idx, color, FILLED, 8, hierarchy);
		}
		
		//For drawing boundary boxes of the contours (Boats)
		/*for (int i = 0; i < contours.size(); i++)
		{
			Rect rect = boundingRect(contours[i]);
			if (rect.area() > 2000 and rect.area() < 250000)
			{
				rectangle(frame, rect, Scalar(255, 0, 0), 2);
			}
		}*/
		
		SimpleBlobDetector::Params params; 	// Setup SimpleBlobDetector parameters.

		params.filterByColor = false;
		
		params.filterByArea = true;
		params.minArea = 1000;
		params.maxArea = 50000;

		params.filterByCircularity = false;

		params.filterByConvexity = false;
		params.minConvexity = 0.87;

		params.filterByInertia = false;
		params.minInertiaRatio = 0.2;


		Mat image_with_keypoints;
		Ptr<SimpleBlobDetector> detector = SimpleBlobDetector::create(params);
		vector<KeyPoint> keypoints;
		detector->detect(contoured, keypoints);
		drawKeypoints(contoured,
			keypoints,
			image_with_keypoints,
			Scalar(0, 0, 255),
			DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
		//output.write(image_with_keypoints);
		imshow("Contours", contoured);
		imshow("Final", image_with_keypoints);
		if (waitKey(30) >= 0)break;
		
	}
	cap.release();
	output.release();
}


// FUNCTION FOR TRACKING OBJECTS INSTEAD OF IDENTIFYING THEM. DOESN'T WORK YET.
void objectTracking()
{
	// Convert to string
	#define SSTR( x ) static_cast< std::ostringstream & >( \
	( std::ostringstream() << std::dec << x ) ).str()

	Ptr<Tracker> tracker;


	tracker = TrackerKCF::create();
		
	// Read video
	VideoCapture video("C:/opencv_images/boats.mp4");

	// Exit if video is not opened
	if (!video.isOpened())
	{
		cout << "Could not read video file" << endl;
		exit(-1);
		waitKey(0);
	}

	// Read first frame 
	Mat frame;
	bool ok = video.read(frame);

	// Define initial bounding box 
	Rect bbox(287, 23, 86, 320);

	// Uncomment the line below to select a different bounding box 
	// bbox = selectROI(frame, false); 
	// Display bounding box. 
	rectangle(frame, bbox, Scalar(255, 0, 0), 2, 1);

	imshow("Tracking", frame);
	tracker->init(frame, bbox);

	while (video.read(frame))
	{
		// Start timer
		double timer = (double)getTickCount();

		// Update the tracking result
		bool ok = tracker->update(frame, bbox);

		// Calculate Frames per second (FPS)
		float fps = getTickFrequency() / ((double)getTickCount() - timer);

		if (ok)
		{
			// Tracking success : Draw the tracked object
			rectangle(frame, bbox, Scalar(255, 0, 0), 2, 1);
		}
		else
		{
			// Tracking failure detected.
			putText(frame, "Tracking failure detected", Point(100, 80), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(0, 0, 255), 2);
		}

		// Display tracker type on frame
		putText(frame, "KCF Tracker", Point(100, 20), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(50, 170, 50), 2);

		// Display FPS on frame
		//putText(frame, "FPS : " + int(fps), Point(100, 50), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(50, 170, 50), 2);

		// Display frame.
		imshow("Tracking", frame);

		// Exit if ESC pressed.
		int k = waitKey(1);
		if (k == 27)
		{
			break;
		}

	}
}