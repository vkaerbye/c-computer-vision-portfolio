# Computer Vision Portfolio

*Author name: Viktor Møller Kærbye*

*Education: IT Technology - C++ for Computer Vision Elective*

*Link to all files used in this project: [GitLab](https://gitlab.com/vkaerbye/c-computer-vision-portfolio)*

## Table of contents
- [Computer Vision Portfolio](#computer-vision-portfolio)
  - [Table of contents](#table-of-contents)
  - [Introduction](#introduction)
  - [Problem Statements](#problem-statements)
    - [Use case 1 - *Detecting animals captured on a wildlife camera*](#use-case-1---detecting-animals-captured-on-a-wildlife-camera)
    - [Use case 2 - *Counting ships sailing through a Strait/Canal*](#use-case-2---counting-ships-sailing-through-a-straitcanal)
    - [Use case 3 - *Sorting beers based on their label*](#use-case-3---sorting-beers-based-on-their-label)
  - [Methodology and process](#methodology-and-process)
    - [Use case 1 -  *Detecting animals captured on a wildlife camera*](#use-case-1----detecting-animals-captured-on-a-wildlife-camera)
      - [Method 1 - *Color thresholding*](#method-1---color-thresholding)
      - [Method 2 - *Edge detection*](#method-2---edge-detection)
      - [Method 3 - *Contouring*](#method-3---contouring)
    - [Use case 2 - *Counting ships sailing through a Strait/Canal*](#use-case-2---counting-ships-sailing-through-a-straitcanal-1)
    - [Use case 3 - *Sorting beers based on their label*](#use-case-3---sorting-beers-based-on-their-label-1)
      - [Method 1 - *Comparing contours*](#method-1---comparing-contours)
        - [Method 2 - *Using SIFT to compare keypoints*](#method-2---using-sift-to-compare-keypoints)
  - [Results and evaluation](#results-and-evaluation)
    - [Use case 1](#use-case-1)
    - [Use case 2 - *Counting ships sailing through a Strait/Canal*](#use-case-2---counting-ships-sailing-through-a-straitcanal-2)
    - [Use case 3 - *Sorting beers based on their label*](#use-case-3---sorting-beers-based-on-their-label-2)
        - [Method 1 - *Comparing Contours*](#method-1---comparing-contours-1)
        - [Method 2 - *Using SIFT to compare keypoints*](#method-2---using-sift-to-compare-keypoints-1)
  - [Appendix](#appendix)
    - [Appendix 1](#appendix-1)


## Introduction
This portfolio aims to explain different use cases of the module **"OpenCV"**. This module is used for image processing and evaluation (*Computer Vision*). The module will, in this case, be used in the programming language **C++**. The OpenCV module is a very powerful tool for processing images, with many built-in functions, automating different processes. It does, however, also contain tools for manually performing these tasks, to further ensure that the outcome is desireable. Both of these methods for image processing will be showcased in this portfolio.

## Problem Statements
*This section describes the reasons for working with each of the use cases, and gives an understanding of why these specific use cases were chosen to showcase what Computer Vision is capable of.*

### Use case 1 - *Detecting animals captured on a wildlife camera*
There are many reasons to put up wildlife cameras in forests or on your property; tracking animals for hunting, capturing vandalising pests or simply for then fun of it, might be a few. But filtering out what is actually an animal in the image can be very useful, to avoid having to look through many different photos of rustling leaves, the family cat passing by, the sun rising and on... Thus, different methods for filtering out animals in photos taken, becomes a very important and useful task - luckily Computer Vision is perfect for this task. 

### Use case 2 - *Counting ships sailing through a Strait/Canal*
Keeping tabs on traffic has always been an important task for both the danish state and townships throughout the country. Whenever traffic is counted, what's almost *always* being counted, is cars. The traffic in a strait or other narrow body of water might also be very interesting for officials, since this data can be used to track when the strait is most populated, thus increasing the risk of nautical traffic accidents. This data could help alert rescue personnel or the coast guard, ensuring that they are on close watch whenever the population rises above a set threshold. 
Setting up a live videofeed and extracting the number of boats in real-time would yield vital important data to the correct officials.

### Use case 3 - *Sorting beers based on their label*
This use case is based on the beer sorting system at the local bar **Bogholderiet** in Svendborg on Fyn. This is a craft beer bar, serving around 290 different craft beers. Keeping tabs on all the beers and their location on the shelves in the huge walk-in refridgerator takes time and effort. Computer Vision can be used to quickly and easily identify a beer based on a picture of its label, according to a template image of the beer in question. Some programming can be implemented to return it's position on the shelves afterwards. 
This type of image recognition could also be implemented in other industries wih many products, in turn informing a robot or automated device, where to place the item. 

## Methodology and process

### Use case 1 -  *Detecting animals captured on a wildlife camera*
The code for this use case can be found here: [GitLab](https://gitlab.com/vkaerbye/c-computer-vision-portfolio/-/blob/main/RatFiltering.h)
#### Method 1 - *Color thresholding*
An approach to detecting an animal in a picture, would be to sort out which color the animal is, and focus solely on that color, ignoring everything else. If a camera was set up to catch rats running across your lawn, brown and black colors are of interest, since most rats are either brown or black. To avoid taking pictures of the grass moving, all green colors can be filtered out.
Doing this in the RGB Color Space *(OpenCV inherently uses BGR, which is the same format, just flipped)* is very difficult, since each pixel contains 3 values for color - Red, Green and Blue. Thus, it must first be converted to a color space, which is easier to handle, when it comes to color. For this purpose **HSV** *(Hue, Saturation, Value)* is perfect, since all data relating to color is saved in the **Hue** parameter. The following code snippet shows the conversion from BGR to HSV.

```
Mat img = imread("C:/opencv_images/rat1.jpg");
Mat hsvimg;
cvtColor(img, hsvimg, COLOR_BGR2HSV);
```

This creates a new image matrix in the *hsvimg* variable, ready to be thresholded.
Next, the image needs to be filtered, and the right colors picked out. 
Since brown and black colors have values between 0 and ~20 in the Hue spectrum, a threshold of 0-20 must be set to create the binary mask, which will in turn filter out grass from the picture.
The following code snippet contains the thresholding, creation of the binary mask and the final filtering of the image:
```
Scalar minThresh = Scalar(0, 0, 0);
Scalar maxThresh = Scalar(20, 255, 255);
	
Mat mask, imageOut, inverseMask;
	
inRange(output, minThresh, maxThresh, mask);
bitwise_not(mask, inverseMask);
bitwise_and(image, image, imageOut, inverseMask);
```
The image before filtering:               | The resulting image after filtering:
:----------------------------------------:|:------------------------------------:
![Imgur](https://i.imgur.com/EIwZcfG.jpg) | ![Imgur](https://imgur.com/ApM127B.jpg)




#### Method 2 - *Edge detection*
When detecting edges, blurring the image is also paramount, since all the individual strands of grass would otherwise be detected as *many* edges, introducing noise into the resulting image. 
There are multiple methods for edge detection, but the general principle is the same; look for places in the image, where the change in color is drastic enough to be an edge. 
A widely used method, built into OpenCV, is **Canny** edge detection, which automatically searches for edges. The following code snippet utilizes this method:

```
dst.create(src.size(), src.type());
cvtColor(src, src_gray, COLOR_BGR2GRAY);
namedWindow(window_name, WINDOW_AUTOSIZE);
blur(src_gray, detected_edges, Size(25, 25));
Canny(detected_edges, detected_edges, lowThreshold, lowThreshold * ratio, kernel_size);
dst = Scalar::all(0);
img.copyTo(dst, detected_edges);
imshow(window_name, dst);
waitKey(0);
return 0;
```

The code above uses different user-made variables, including **lowThreshold**, **ratio**, **window_name** and **kernel_size**. These are defined as such:

```
int lowThreshold = 10;
const int ratio = 3;
const char* window_name = "Edge Map";
const int kernel_size = 3;
```

When the above code is run, the following results are obtained:
The image before processing:            | The image after processing:
:--------------------------------------:|:--------------------------------------:
![Imgur](https://imgur.com/S0byPro.jpg) | ![Imgur](https://imgur.com/lVz3Jh7.jpg)
![Imgur](https://imgur.com/ktzlu9z.jpg) | ![Imgur](https://imgur.com/QOg3ME3.jpg)
![Imgur](https://imgur.com/7ClMLRv.jpg) | ![Imgur](https://imgur.com/fogVZ7Z.jpg)

#### Method 3 - *Contouring*
As seen in **Method 2**, finding only edges doesn't make much sense, since these edges aren't connected. This way, the outlines of the rat (or item in question) aren't shown clearly. These edges can, however, be connected using other methods, thus showing the ***contours*** of the object. 
In regards to the images in question here, where a lot of grass, and thus, a lot of edges are present, this method is less than ideal, but can prove useful nonetheless.
The following code snippet shows the processing made to the image to extract the contours:
```
Canny(imgBlur, edges, 10, 30);
dilate(edges, dilated, kernel);

findContours(dilated,
  contours,
  hierarchy,
  RETR_EXTERNAL,
  CHAIN_APPROX_SIMPLE);

int idx = 0;
for (; idx >= 0; idx = hierarchy[idx][0])
{
  Scalar color(rand() & 255, rand() & 255, rand() & 255);
  drawContours(contoured, contours, idx, color, FILLED, 8, hierarchy);
}
imshow("Components", contoured);
waitKey(0);
return contoured;
```
The code above *dilates* the foud edges, effectively enlarging them, so that they may meet each other and create a shape which can be filled. These contours are then drawn *(The drawContours function)*, with the **FILLED** keyword as an argument in the function, causing completed contours to be filled with a random color *(defined on the line just above, where a Scalar value is defined as 3 random values between 0 and 255)*.
Below the original and resulting images are shown:

The image before processing:            | The image after processing:
:--------------------------------------:|:--------------------------------------:
![Imgur](https://imgur.com/S0byPro.jpg) | ![Imgur](https://imgur.com/QaSGdnX.jpg)
![Imgur](https://imgur.com/ktzlu9z.jpg) | ![Imgur](https://imgur.com/XZYkEfw.jpg)
![Imgur](https://imgur.com/7ClMLRv.jpg) | ![Imgur](https://imgur.com/ZMytjNF.jpg)

### Use case 2 - *Counting ships sailing through a Strait/Canal*
The code for this use case can be found here: [GitLab](https://gitlab.com/vkaerbye/c-computer-vision-portfolio/-/blob/main/boatDetector.h)

The video which will be analysed in this chapter can be found here: [Youtube](https://youtu.be/fX0I5pAJEAE).

In order to find ships in a video, each frame of the video must be analyzed and the ships picked out. This is done easiest by contouring the image after using Canny thresholding to extract edges from it. The following code snippet is a function called **preprocessing**, which *(you guessed it!)* preprocesses the image, preparing it for for analyzing:

```
Mat preprocessing(Mat img) {

    Mat imgGray, imgBlur, edges, dilated, eroded;

    cvtColor(img, imgGray, COLOR_BGR2GRAY);
    GaussianBlur(img, imgBlur, Size(5, 5), 3, 0);
    Canny(imgBlur, edges, 25, 75);

    Mat kernel = getStructuringElement(MORPH_RECT, Size(5, 5));

    dilate(edges, dilated, kernel);
    erode(dilated, eroded, kernel);

    return eroded;
}
```
In short, the code above converts an image to grayscale, blurs it with a 5-by-5 kernel, finds edges using Canny edge detection, dilates the found edges to create coherent shapes, erodes the resulting image, to seperate any contours that might have "melted" together, and finally returns the resulting image. 

This preprocessing must be applied to every frame in the video in question, so the video file needs to be loaded into OpenCV. The following code snippet loads a cropped version of the video file called **"boats_cropped.mp4"** into OpenCV, checks if the file was loaded correctly, and loops until no more frames remain: 
```
VideoCapture cap("C:/opencv_images/boats_cropped.mp4");
 Mat frame;
vector<vector<Point>> contours;
vector<Vec4i> hierarchy;
namedWindow("Contours", WINDOW_AUTOSIZE);
if (!cap.isOpened()) { 
  cerr << "Could not open video file!" << endl;
  exit(0); 
}
while (cap.read(frame)) {
  Mat contoured = Mat::zeros(frame.rows, frame.cols, CV_8UC3);
  Mat proc = preprocessing(frame);
```
The reason for using a cropped version of the video file, is to eliminate as much noise and as many distractions as possible, to avoid false positives, when looking for ships. The vectors **hierarchy** and **contours** will be used by the **findContours** function in the next lines:

```
findContours(proc,
  contours,
  hierarchy,
  RETR_EXTERNAL,
  CHAIN_APPROX_SIMPLE);
int idx = 0;
for (; idx >= 0; idx = hierarchy[idx][0])
{
  Scalar color(rand() & 255, rand() & 255, rand() & 255);
  drawContours(contoured, contours, idx, color, FILLED, 8, hierarchy);
}
```
The code above finds all the contours in the frame and draws the top-level contours onto an image called **contoured**. Each coherent contour will get its own random color. 
Now that an image has been created showing *only* the contours of the frame, a function can be applied to single out all coherent contours and outline ones that fit given criterias.
This function is called **simpleBlobDetector**, and can take many parameters as inputs to filter the "blobs" it finds. The function can filter by: *Size, Circularity, Convexity, Inertia, and Color*. Since the contours all get random colors, filtering by color doesn't make much sense. Ships aren't exactly circular either, so filtering by circularity would rule out too many would-have-been ships detected. Convexity refers to the curvature of an object, and had we been tracking *sails* this could have been a viable option to filter by object which have the curvature of a sail. Inertia is a lot harder to grasp, but doesn't seem to help with tracking ships, even though they do actually have a certain amount of intertia when moving through the picture. 
The final criteria; Size, does however help identify ships. Since water is a very volatile substance, waves and reflections create their own small contours, which the simpleBlobDetector will pick up, if no filtering is added. Thus, filtering by size *(Figuring out the correct size parameters was done by trial and error)*, leaves the image with keypoints tracking **mainly** ships. 
The code snippet below shows this process:
```
SimpleBlobDetector::Params params;

params.filterByColor = false;

params.filterByArea = true;
params.minArea = 1000;
params.maxArea = 50000;

params.filterByCircularity = false;

params.filterByConvexity = false;

params.filterByInertia = false;

Mat image_with_keypoints;
Ptr<SimpleBlobDetector> detector = SimpleBlobDetector::create(params);
vector<KeyPoint> keypoints;
detector->detect(contoured, keypoints);
drawKeypoints(contoured,
  keypoints,
  image_with_keypoints,
  Scalar(0, 0, 255),
  DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
imshow("Contours", contoured);
imshow("Final", image_with_keypoints);
if (waitKey(30) >= 0)break;
```
The **drawKeypoints** function adds circles to the original frame, corresponding to the size of the blob it detected. 
The resulting video can be found using this [link](https://youtu.be/ZBdcYtKEHK8). I recommend slowing the video down a bit, to get a better grip of which contours the algorithm has tracked. 

To get a better understanding of what the simpleBlobDetector algorithm is seeing, a video has been created showing the contours that the algorithm tracks, with the keypoints it found drawn onto it. This video can be seen [here](https://youtu.be/xRIVHEtlyFM).

### Use case 3 - *Sorting beers based on their label*
The full code for this solution can be found here: [GitLab](https://gitlab.com/vkaerbye/c-computer-vision-portfolio/-/blob/main/beerDetector.h)

#### Method 1 - *Comparing contours*
This use case combines the methods used in the previous use cases to extract the contours on the beer and its label. It will then compare the found contours to a library of template contours, which have been made in ideal conditions, to determine which beer is on the photo. 
This poses a series of complications, mostly regarding that the images aren't exactly the same sizes and with the same backgrounds. 

The program works by receiving a folder of images *(all in ".jpg" format)*, which it will iterate through until no more images remain. This is shown in the following code snippet:
```
vector<double> ans;
vector<vector<Point>> templateList[7];
int count = 0;
for (const auto& dirEntry : recursive_directory_iterator("C:/opencv_images/beerTemplates/")) {
  string fname = "C:/opencv_images/beerTemplates/" + get_stem(dirEntry.path()) + ".jpg";
  Mat image = imread(fname);
  templateList[count] = extractContours(image);
  count++;
  cout << "Converted: " << dirEntry << std::endl;
}
```
The code above calls upon a function called **extractContours()**. This function is used to extract contours from images. The parameters for extracting these contours are the same for the template images as it is for the "real life" images, ensuring that contours are extracted as equally as possible, to avoid confusion. **extractContours()** is shown in the code snippet below:


```
vector<vector<Point>> extractContours(Mat src) {
	Mat tempGray, tempBlur, tempEdges, tempDilated, tempEroded;
	Mat contoured = Mat::zeros(src.rows, src.cols, CV_8UC3);
	Mat kernel = getStructuringElement(MORPH_RECT, Size(2, 2));
	vector<vector<Point>> contours;
	vector<Vec4i> hierarchy;
	cvtColor(src, tempGray, COLOR_BGR2GRAY);
	GaussianBlur(tempGray, tempBlur, Size(3, 3), 3, 0);
	Canny(tempBlur, tempEdges, 235, 250);
	dilate(tempEdges, tempDilated, kernel);
	//erode(tempDilated, tempEroded, kernel);
	findContours(tempDilated,
		contours,
		hierarchy,
		RETR_EXTERNAL,
		CHAIN_APPROX_SIMPLE);
	return contours;
}
```


The code basically just extracts contours from images using the techniques described in **use case 1** and **use case 2**. 
Were these contours to be drawn as an image in white, the following templates are obtained:
Template image            | Resulting contours
:--------------------------------------:|:--------------------------------------:
![Imgur](https://imgur.com/9rBKFdg.jpg) | ![Imgur](https://imgur.com/ayHNiuP.jpg)
![Imgur](https://imgur.com/UuVianR.jpg) | ![Imgur](https://imgur.com/UfBbAis.jpg)
![Imgur](https://imgur.com/Hq1QC9d.jpg) | ![Imgur](https://imgur.com/wBZ84kV.jpg)

This exact contour extraction is then used on an photo of a beer, and the contours are then compared to each of the template images in turn to determine which template the photo resembles the most. This is done with the following code:
```
vector<vector<Point>> imageContours = extractContours(src);
cout << "Loaded image successfully and found " << to_string(imageContours.size()) << " Contours" << endl;


double results[7];
double smallestNum = 1000;
for (int l = 0; l < 7; l++) {
  if (imageContours.size() < templateList[l].size()) {
    for (int k = 0; k < imageContours.size(); k++) {
      double sim = matchShapes(templateList[l][k], imageContours[k], CONTOURS_MATCH_I1, 0);
      if(sim < 1000) ans.push_back(sim);
    }
  }
  else {
    for (int k = 0; k < templateList[l].size(); k++) {
      double sim = matchShapes(templateList[l][k], imageContours[k], CONTOURS_MATCH_I1, 0);
      if (sim < 1000) ans.push_back(sim);
    }
  }
```

The **matchShapes** function returns a value describing how similar each found contour in the photo is, compared to the corresponding one in the template image. The closer this value is to 0, the more alike the contours are. This value is then added to a vector of these numbers. The average value of this vector is then calculated to determine the overall likeness of the image and each template.
After a mean value has been found for each template (compared to the image), the lowest value is stored in a variable called **smallestNum**. *(The smallest resemblance-number is the template which the image resembles the **most**)*

Since the templates are loaded into the program in a specific order, the resulting resemblance value corresponds to a specific location in the list of templates, meaning the beer's name can be returned as such:

```
string beerName;
if (smallestNum == results[0]) beerName = "Albani Odense Classic";
if (smallestNum == results[1]) beerName = "Tuborg Grøn";
if (smallestNum == results[2]) beerName = "Carlsberg Pilsner";
if (smallestNum == results[3]) beerName = "Anarkist Bloody Weizen";
if (smallestNum == results[4]) beerName = "Westmalle Trappist Dubbel";
if (smallestNum == results[5]) beerName = "Slots Classic";
if (smallestNum == results[6]) beerName = "Chouffe Mc Chouffe";

return(beerName);
```


##### Method 2 - *Using SIFT to compare keypoints*
Another method for matching objects, is the algorithm **SIFT**. SIFT is short for Scale-Invariant Feature Transform, and works by selecting **Keypoints** that it thinks could be of interest in a picture. These keypoints contain information about edges, shapes and contours and can be compared to other arrays of Keypoints. This way, even with a noisy background in one image, the main features of the object will still get recognized. 
The following code snippet shows the program iterating through template images in a folder, extracting Keypoints from them and calculating their **Descriptors**, which are the objects the matching algorithm can compare:
```
for (const auto& dirEntry : recursive_directory_iterator("C:/opencv_images/beerTemplates/")) {
  string fname = "C:/opencv_images/beerTemplates/" + get_stem(dirEntry.path()) + ".jpg";
  Mat image = imread(fname, COLOR_BGR2GRAY);
  detector->detect(image, keypoints_templ);
  detector->compute(image, keypoints_templ, descriptors_templ);
  keypoints_templ_list.push_back(descriptors_templ);
  keypoints_templ.clear();
  count++;
  cout << "Converted: " << dirEntry << std::endl;
}
```

As with contour comparison, a vector of comparable objects is created in order to save the template data for later comparison with the image file.

The image is loaded into the program in a manner much like this, and the following code snippet shows the comparison process:

```
FlannBasedMatcher matcher;
vector<DMatch> good_matches;
vector<vector<DMatch>> good_matches_list;
for (int t = 0; t < keypoints_templ_list.size(); t++) {
  cout << "Comparing image to template " << to_string(t + 1) << "..." << endl;
  vector<DMatch> matches;
  matcher.match(descriptors_image, keypoints_templ_list[t], matches);


  double max_dist = 0; double min_dist = 100;
  for (int i = 0; i < descriptors_image.rows; i++)
  {
    double dist = matches[i].distance;
    if (dist < min_dist) min_dist = dist;
    if (dist > max_dist) max_dist = dist;
  }
  
  for (int i = 0; i < descriptors_image.rows; i++)
  {
    if (matches[i].distance <= max(2 * min_dist, 0.02))
    {
      good_matches.push_back(matches[i]);
    }
  }
  good_matches_list.push_back(good_matches);
  good_matches.clear();
```
The two for loops within the first for loop calculate distances between the keypoints and ensure that only keypoints with a given distance threshold are added to the vector of matches, **good_matches**. 
The vector of good matches is then added to a vector of vectors of matches called **good_matches_list**. This ensures, that the good matches are added with indexes corresponding to the template image with which they were compared. 

The index with the highest number of good matches is then found and the number of matches saved in a variable called **high_match**.
This number of matches can then be compared to the vector of vectors of matches to get the corresponding index, and thus, the template of the beer:
```
string beerName;
if (high_match == good_matches_list[0].size()) beerName = "Albani Odense Classic";
if (high_match == good_matches_list[1].size()) beerName = "Tuborg Grøn";
if (high_match == good_matches_list[2].size()) beerName = "Carlsberg Pilsner";
if (high_match == good_matches_list[3].size()) beerName = "Anarkist Bloody Weizen";
if (high_match == good_matches_list[4].size()) beerName = "Westmalle Trappist Dubbel";
if (high_match == good_matches_list[5].size()) beerName = "Slots Classic";
if (high_match == good_matches_list[6].size()) beerName = "Chouffe Mc Chouffe";

return beerName;
```

## Results and evaluation
### Use case 1
Setting a color threshold is very individual based on both usage and image. Even though the usage in the following two examples is the same (detecting rats on a grass lawn), the results are very different when using different images.

<p float="left">
<img src="https://imgur.com/EIwZcfG.jpg" alt="Imgur" width="49%">
<img src="https://imgur.com/ApM127B.jpg" alt="Imgur" width="49%">
</p>

*Example 1: Using a Hue threshold of 0-25 filtered out the rat in the image near perfectly*

<p float="left">
<img src="https://imgur.com/OoMu4P8.jpg" alt="Imgur" width="49%">
<img src="https://imgur.com/nzx6xNA.jpg" alt="Imgur" width="49%">
</p>

*Example 2: Using the same threshold as example 1 yielded very different, if not unusable results*

This shows how individual color thresholding must be set, when searching for object based on their color. In conclusion, color thresholding might not be the perfect solution for this problem.

The images seen in the **Methodology and Process** chapter under *Use Case 1*, show perfectly well how difficult image processing is. None of the methods performed perfectly on all three pictures, indicating that, to process an image perfectly, several methods and algorithms must be combined. 

### Use case 2 - *Counting ships sailing through a Strait/Canal*
While the solution made for identifying ships in the canal does identify **most** ships in the video, it still gets quite confused by other artefacts in the video, such as background scenery, waves/distortions/reflections in the water, thus increasing the number of detected ships to a much higher level than the actual number of ships. Blurring the image further, to remove some of this noise, causes the outlines of ships to become too blurred to detect, thus not creating a solid shape to track. *(See Appendix X and Y)*. Increasing the minimum size threshold in the simpleBlobDetector to avoid detecting these water disturbances, causes ships that are far away to get filtered out, since the blobs aren't large enough. 
Another problem which was encountered when detecting the ships, was a lack of processing power *(or simply a too high-maintenance algorithm)*, seeing as the program was only able to process around 1-2 images pr. second. This ruins the real-time aspect of counting the ships, and could be solved by using a lighter detection algorithm, or by simply upgrading to a more powerful computer running the program. 

Another way of solving this problem, would be to use **object-tracking**. This is much lighter in terms of processing power, and solutions to track multiple objects at once have been created by the OpenCV community. These tracking solutions are not yet implemented in the official release of OpenCV, but might be very soon.
If tracking was used, only 1 in 30 frames (1 time pr second), would have to be analyzed to *detect* the ship, while the tracking algorithm would then track each ship registered. Thus, the heavy load on the computer's processor would be greatly reduced.


### Use case 3 - *Sorting beers based on their label*
##### Method 1 - *Comparing Contours*
A big problem with just taking a picture of a bottle and expecting the algorithm to identify the beer from that photo is the unpredictable **noise**, such as *background*, *lighting* and *reflections*. As an example these photos of beers *"in the wild"* have been contoured using the same parameters as the templates:
Photo of bottle                         | Resulting contours
:--------------------------------------:|:--------------------------------------:
![Imgur](https://imgur.com/p6aZQbr.jpg) | ![Imgur](https://imgur.com/co0zXop.jpg)
![Imgur](https://imgur.com/6k9bXGI.jpg) | ![Imgur](https://imgur.com/ktfAzmy.jpg)
![Imgur](https://imgur.com/2Zmz7MO.jpg) | ![Imgur](https://imgur.com/bGz42ti.jpg)

This causes a lot of false positives to be found, since part of the scenery could be mistaken for being an important part of the bottle. 
From my own discoveries when testing this program, **everything** seems to resemble an Albani Odense Classic, rendering the program rather useless. 
Some of this could be fixed by **blurring** the photos even more before extracting the contours, but seeing as the smallest details in the label or bottle shape could mean that it's a totally different beer, this isn't a viable option. 
Another option would be to give the program more templates to work with, for example photos from various angles, and comparing the photo in question to each of them, and then calculating the mean value of this, but doing so, would cause the program to run very slowly.
Thus, the only thing to do, is to **control the environment**. This means eliminating noisy elements by changing the *physical* world to better suit the needs of the solution. For that purpose, I have created a **"Photo-box"** *(See Appendix 1)*, which is basically just a white box with mounted LED lights, ensuring a constant and equal light on the subject within the box. As the box is white, the background will always be the same color and won't have any noise. 

##### Method 2 - *Using SIFT to compare keypoints*
A depiction of how SIFT compares Keypoints can be seen below:
<p float="left">
<img src="https://imgur.com/EOr1D8Q.jpg" width="40%">
<img src="https://imgur.com/iD0Le6b.jpg" width="59%">
<img src="https://imgur.com/zu68xxg.jpg">
</p>
This shows that SIFT is indifferent towards differences in size, rotation and background of the template and source images.

The results of the program running are as follows:
Photo input                         | Return value
:------------------------------------:|:--------------------------------------:
![Imgur](https://imgur.com/p6aZQbr.jpg) | "The beer depicted is: Tuborg Grøn"
![Imgur](https://imgur.com/6k9bXGI.jpg) | "The beer depicted is: "Carlsberg Pilsner"
![Imgur](https://imgur.com/ljuJLgO.jpg) | "The beer depicted is: Anarkist Bloody Weizen"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

As seen in the table above, the algorithm isn't perfect *(Westmalle Trappist Dubbel is recognized as Carlsberg Pilsner),* which i have found to relate to the image's size *(resolution)*. It seems that, with more pixels, come more Keypoints, increasing the possibility for false positives. 

This problem could simply be fixed by resizing images larger than a given threshold, or by taking images of the bottles and the templates with the same camera, ensuring that templates and images for comparison always have the same dimensions. 

Another issue with using SIFT for the purpose described in the problem statement, is the resource cost on the computer running the program. When comparing larger image files, RAM *(Random Access Memory)* usage is upwards of **3GB** and images take around **15 seconds** to compare to each template. Comparing a single image to 285 template images would take **1 hour, 11 minutes and 15 seconds**. This isn't suitable, as beers going into the fridge should be sorted ***MUCH*** faster. 

## Appendix
### Appendix 1
A 3D file in STL format can be found here: [GitLab](https://gitlab.com/vkaerbye/c-computer-vision-portfolio/-/blob/main/Beer%20Box%20v2.stl)

Controlling physical environment with a *"Photo-Box"* to reduce noise in images
<p float="left">
<img src="https://imgur.com/wXoAYYn.jpg" width="49%">
<img src="https://imgur.com/gRgNs7P.jpg" width="49%">
<img src="https://imgur.com/MNI3Xhn.jpg" width="49%">
<img src="https://imgur.com/ZPWjB8u.jpg" width="49%">
</p>